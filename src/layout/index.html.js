//API -- https://jsonplaceholder.typicode.com/

//Нужно найти в документации jsonplaceholder где лежат тудушки
const url = 'https://jsonplaceholder.typicode.com'
const rawResponse = await fetch(url + '/todos', {
  method: 'GET'
});

//Нужно привести сырой ответ сервера в формат JSON
const todos = await rawResponse.json()

/**
 * Функция ответственная за рендеринг ответа с сервера
 */
const renderedTodos = todos.map((todo) => {
  /**
   Ваш код
   ! Обратите внимание, что если JSONPlaceholder отдает завершенную
   тудушку, то мы должны помечать это через атрибут checked для элемента input
  */
  let div = document.createElement('div')
  div.innerText = todo.title

  let checkbox = document.createElement('input')
  checkbox.type = 'checkbox'
  if (todo.completed) {
    checkbox.setAttribute('checked', true)
  }

  div.appendChild(checkbox)

  return div
});

//Элемент, к которому нужно добавить отрендеренные тудушки
//К этому элементу в дальнейшем будет применена функция createElement
const app = {
  tag: 'div',
  child: [],
};

app.child = renderedTodos;

export default app;